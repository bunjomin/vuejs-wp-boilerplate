import api from '../../../api'
import * as types from '../../mutation-types'

// initial state
const state = {
  all: [],
  loaded: false
}

// getters
const getters = {
  // Returns an array all categories
  primaryMenu: state => state.all,
  primaryMenuLoaded: state => state.loaded
}

// actions
const actions = {
  getPrimaryMenu ({ commit }) {
    api.getMenu('primary-nav', (menu) => {
      commit(types.STORE_FETCHED_PRIMARY_MENU, { menu })
      commit(types.PRIMARY_MENU_LOADED, true)
      commit(types.INCREMENT_LOADING_PROGRESS)
    })
  }
}

// mutations
const mutations = {
  [types.STORE_FETCHED_PRIMARY_MENU] (state, { menu }) {
    state.all = menu
  },

  [types.PRIMARY_MENU_LOADED] (state, bool) {
    state.loaded = bool
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
