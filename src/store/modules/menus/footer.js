import api from '../../../api'
import * as types from '../../mutation-types'

// initial state
const state = {
  all: [],
  loaded: false
}

// getters
const getters = {
  // Returns an array all categories
  footerMenu: state => state.all,
  footerMenuLoaded: state => state.loaded
}

// actions
const actions = {
  getFooterMenu ({ commit }) {
    api.getMenu('footer-nav', (menu) => {
      commit(types.STORE_FETCHED_FOOTER_MENU, { menu })
      commit(types.FOOTER_MENU_LOADED, true)
      commit(types.INCREMENT_LOADING_PROGRESS)
    })
  }
}

// mutations
const mutations = {
  [types.STORE_FETCHED_FOOTER_MENU] (state, { menu }) {
    state.all = menu
  },

  [types.FOOTER_MENU_LOADED] (state, bool) {
    state.loaded = bool
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
