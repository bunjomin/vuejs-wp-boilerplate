<?php

function api_get_menu (WP_REST_Request $request) {
  if ( !isset($request['slug']) ) { return false; }
  return wp_get_nav_menu_items($request['slug']);
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'endpoint', '/menu', [
    'methods' => 'GET',
    'callback' => 'api_get_menu'
  ] );
});

?>
